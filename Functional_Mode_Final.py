#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import tensorflow as tf
from tensorflow import keras
import pandas as pd
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import tensorflow_model_optimization as tfmot
import autokeras as ak


# In[3]:


dataframe1 = pd.read_excel('Irrisamp_ref.xls', header=0)
print(dataframe1.shape)
data = dataframe1.values
data = data.astype('float32') 
#data = data.astype('int8') 
xs = data[:, -2:]
ys = data[:, 0]


# In[4]:


#create the input layer
input_layer = keras.Input(shape=(2, ))
 
#create two hidden layers with shared input layer
hidden_1 = keras.layers.Dense(64, activation='relu')(input_layer)
hidden_2 = keras.layers.Dense(64, activation='relu')(input_layer)
 
#merge the two layers 
merged_layers = keras.layers.concatenate([hidden_1, hidden_2])
 
#built the rest of the layers
hidden_3 = keras.layers.Dense(32, activation='relu')(merged_layers)
output = keras.layers.Dense(1, activation='linear')(hidden_3)
 
model = keras.Model(inputs=input_layer, outputs=output)


# In[5]:


model = keras.Model(inputs=input_layer, outputs=output)


# In[6]:


model.summary()


# In[7]:


model.compile(optimizer='adam', loss='mse')
history = model.fit(xs, ys, epochs=200)


# In[8]:


model.save("functional_mod")


# In[9]:


X_new = np.asarray([[6,5]]).astype('float32')
new_y = model.predict(X_new)
new_y


# In[12]:


train_ids = tf.data.Dataset.from_tensor_slices(xs).batch(1)
def representative_data_gen():
    for input_value in train_ids.take(50):
        yield [input_value]
#def apply_quantization_to_layer(layer):
#    print("layer ", layer)
#    if isinstance(layer, ak.keras_layers.MultiCategoryEncoding):
#            return layer
#    return tfmot.quantization.keras.quantize_annotate_layer(layer)



model = tf.keras.models.load_model("functional_mod")
#annotate_model = tf.keras.models.clone_model(model, clone_function=apply_quantization_to_layer)
#quantize_aware_model = tfmot.quantization.keras.quantize_apply(annotate_model)
#quantize_aware_model.summary()

converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.optimizations = [tf.lite.Optimize.DEFAULT]
converter.representative_dataset = representative_data_gen
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8, tf.lite.OpsSet.SELECT_TF_OPS]
converter.allow_custom_ops = True
converter.experimental_new_converter =True
converter.inference_input_type = tf.int8
converter.inference_output_type = tf.int8
tflite_model = converter.convert()

with open('model_ref.tflite', 'wb') as f:
  f.write(tflite_model)


# In[ ]:




