#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
import tflite_runtime.interpreter as tflite
import requests
import datetime
import arrow


# Load the TFLite model and allocate tensors.
interpreter = tflite.Interpreter(model_path="./model_ref_edgetpu.tflite",experimental_delegates=[tflite.load_delegate('libedgetpu.so.1')])
#interpreter.resize_tensor_input(0, [1, 2], strict=False)
# Get input and output tensors.
interpreter.allocate_tensors()
input_details = interpreter.get_input_details()[0]
output_details = interpreter.get_output_details()[0]

Kc_val = 1.15

def getEstimateRainFall():
    # api-endpoint
    start_time = arrow.utcnow()
    end_time = str(start_time.shift(hours=12))
    
    print(start_time)
    print(end_time)
    
    #have to settle on 'from-time' and 'to-time'
    URL = "https://atsuyatechnologies_satagopan:0AyLOxgMn3R4b@api.meteomatics.com/" + str(start_time )+ "--" + end_time + ":PT5M/rain_water:kgm2/13.00645,80.2577791/json?model=mix"
    # defining a params dict for the parameters to be sent to the API
    PARAMS = {}
      
    # sending get request and saving the response as response object
    r = requests.get(url = URL, params = PARAMS)
      
    # extracting data in json format
    data = r.json()['data'][0]['coordinates'][0]['dates']
    
    sum = 0
    counter = 0
    for value in data:
        sum += value['value']
        counter += 1
    
    avg_rainfall = sum/counter 
    return avg_rainfall
    
    #put the avg_rainfall in db

def getEvapoTransRates():
    
    end_time = arrow.utcnow()
    start_time = str(end_time.shift(hours=-1))

    #api-endpoint
    URL = "https://atsuyatechnologies_satagopan:0AyLOxgMn3R4b@api.meteomatics.com/"+ start_time + "--" + str(end_time) + ":PT5M/evapotranspiration_1h:mm/13.00645,80.2577791/json?model=mix"
    PARAMS = {}
    
    # sending get request and saving the response as response object
    r = requests.get(url = URL, params = PARAMS)
    
    # extracting data in json format
    data = r.json()['data'][0]['coordinates'][0]['dates']
        
    sum = 0
    counter = 0
    for value in data:
        sum += value['value']
        counter += 1
    
    ETo = sum
    ETc = ETo * Kc_val
    return ETc

avg_rainfall = getEstimateRainFall()
avg_transrate = getEvapoTransRates()

print ("input", input_details)
# Test the model on random input data.
input_data = np.array([50,10])#float32)

input_scale, input_zero_point = input_details["quantization"]
input_data = input_data / input_scale + input_zero_point

input_data = np.expand_dims(input_data, axis=0).astype(input_details["dtype"])

print("input data", input_data)

interpreter.set_tensor(input_details['index'], input_data)

interpreter.invoke()

# The function `get_tensor()` returns a copy of the tensor data.
# Use `tensor()` in order to get a pointer to the tensor.
output_data = interpreter.get_tensor(output_details['index'])[0]
print(output_data)
#print('odata argmax', output_data.argmax())
#print('o details', output_details)

output_scale, output_zero_point = output_details["quantization"]
output_data = (output_data - output_zero_point) *  output_scale
#output_data = output_data *output_scale - output_zero_point

I = output_data - avg_rainfall
print('Avg rainfall=',avg_rainfall)
print('I0 = ',output_data)
l = 700
b = 600
farm_area = l*b
I = np.round((I*farm_area)/1000000,2)
if(I<0):
	print("No need to irrigate the farm !")
else:
	print("Your tomato farm needs", I[0], "litres of water !")

#print('odata argmax', output_data.argmax())
#print('odata argmax', output_data.argmin())
